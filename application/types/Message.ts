export interface Message {
    message: string,
    picture: string,
    key: string,
}