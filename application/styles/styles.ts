import { StatusBar, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: StatusBar.currentHeight,
    },

    containerFullInput: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8',
    },

    containerInput: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '80%',
    },

    input: {
        backgroundColor: '#e8e8e8',
        width: '100%',
        padding: 10,
        borderRadius: 15,
        fontSize: 17,
    },

    containerButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '80%',
    },

    button: {
        backgroundColor: '#ffed4e',
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginVertical: 5,
        borderRadius: 15,
    },

    buttonDisabled: {
        opacity: 0.5,
        backgroundColor: '#ffed4e',
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginVertical: 5,
        borderRadius: 15,
    },

    textButton: {
        color: '#fff',
        fontWeight : 'bold',
    },

    counter: {
        fontSize: 17,
        color: '#a6a6a6',
        marginLeft: 5,
    },

    containerList: {
        flex: 6,
        alignItems: 'center',
        // justifyContent: 'center',
        width: '100%'
    },

    fullLength: {
        width: '98%',
    },

    fullLengthAlign: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },

    text: {
        flexWrap: 'wrap',
    },

    image: {
        width: 300,
        height: 200,
        resizeMode: 'cover',
        marginBottom: 10,
    },

    fullLengthItem: {
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },

    item: {
        flexGrow: 0,
        marginTop: 15,
        width: '80%',
    },
});

export default styles;