import React, { useEffect } from 'react';
import { ListRenderItem, StyleSheet, Text, TextInput, TouchableOpacity, TouchableOpacityBase, View, Image, FlatList, Button } from 'react-native';

import { Message } from './types/Message';

import {Picture} from './types/Image'
import styles from './styles/styles'

interface Props {
    listMessageProps: Message[],
}

const ListMessage = ( {listMessageProps} : Props ) : JSX.Element => {

    // Hook Ref pour y stocker la flatlist
    const flatListRef = React.useRef<FlatList>(null);

    // Fonction pour créer un template des "items" (messages) à afficher dans la flatlist
    const renderItem: ListRenderItem<Message> = ({ item }) : JSX.Element => {
        return(
            <View style = {styles.fullLengthItem}>
                <View style = {styles.item}>
                    <Text style = {styles.text}>{item.message}</Text>
                    <Image
                        style = {styles.image}
                        source = {{
                            uri: item.picture,
                        }}
                    />
                </View>
            </View>
        )
    }

    // Hook Effect qui change en fonction du props listMessageProps et active l'autoscroll
    useEffect(() => {
        if(listMessageProps.length > 0) {
           flatListRef.current!.scrollToIndex({ animated: true, index: 0 }) 
        }
    }, [listMessageProps.length])
    
    return (
        <View style={styles.fullLength}>
            <FlatList
                    ref={flatListRef}
                    data={listMessageProps}
                    renderItem={renderItem}
            />
            
        </View>
        );
    }

    export default ListMessage;