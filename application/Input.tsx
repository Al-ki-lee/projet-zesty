import axios from 'axios';
import React from 'react';
import { Keyboard, StyleSheet, Text, TextInput, TouchableOpacity, TouchableOpacityBase, View } from 'react-native';

import {Picture} from './types/Image'
import styles from './styles/styles'
import {apiImage} from './config'

interface Props {
    newMessage: (message: string, picture: string) => void,
}

const Input = ({ newMessage } : Props) : JSX.Element => {

    // Hook State pour stocker le texte et si le bouton est désactivé ou non
    const [text, setText] = React.useState<string>('');
    const [isDisabled, setIsDisabled] = React.useState(false);

    // fonction pour récupérer l'image via l'api et publier le message
    const onPress = () => {
        // Bloque le bouton "publier" le temps de la requête
        setIsDisabled(true);
        
        axios.get<Picture>(apiImage)
            .then((response) => {
                // Fonction newMessage() et activateScroll() du composant App passées en Props
                newMessage(text, response.data.file);

                // Passe havePressed et isDisabled en false, vide le texte stocké et désactive le clavier
                setIsDisabled(false);
                setText('');
                Keyboard.dismiss();
            })
            .catch((error) => {
                console.log(error);
                setIsDisabled(false);
            })
    }
    
    return (
        <View style={styles.fullLengthAlign}>
            <View style={styles.containerInput}>
                <TextInput
                style={styles.input}
                    onChangeText={setText}
                    value={text}
                    placeholder='Quoi de neuf ?'
                    maxLength={90}
                />  
            </View>

            <View style={styles.containerButton}>
                <Text style={styles.counter}>{text.length} / 90</Text>
                <TouchableOpacity
                    style={(text.length > 0) ? styles.button : styles.buttonDisabled}
                    onPress={onPress}
                    disabled={!(text.length > 0 && !isDisabled)}
                >
                    <Text style={styles.textButton}>Publier</Text>
                </TouchableOpacity>
            </View>
            
            
        </View>
    );
}

export default Input;