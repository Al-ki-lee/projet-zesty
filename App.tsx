import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { FlatList, ListRenderItem, StyleSheet, Text, View, Image, KeyboardAvoidingViewBase, KeyboardAvoidingView, Platform } from 'react-native';

import Input from './application/Input';
import ListMessage from './application/ListMessage';

import {Message} from './application/types/Message'
import styles from './application/styles/styles'

const App = () : JSX.Element => {

    // Hook state pour stocker les messages, les "keys" des messages
    // et le booléen qui active l'autoscroll (Props pour le composant ListMessage)
    const [listMessage, setListMessage] = React.useState<Message[]>([]);
    const [totalKey, setTotalKey] = React.useState<number>(0);

    // Fonction pour ajouter un message dans la liste (passé en props au composant Input)
    const newMessage = (message: string, picture: string) => {
        let messageObject = {
            message: message,
            picture: picture,
            key: totalKey.toString()
        }

        // Incrémente les "keys" pour le prochain message
        setTotalKey(totalKey + 1);

        // Ajoute le message au début de la liste
        setListMessage([messageObject,...listMessage]);
    }
    
    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            keyboardVerticalOffset={Platform.OS == "ios" ? 0 : 20}
            enabled={Platform.OS === "ios" ? true : false}
            style = {styles.mainContainer}
        >
            <View style = {styles.containerFullInput}>
                <Input
                    newMessage = {newMessage}
                />
            </View>
            <View style = {styles.containerList}>
                <ListMessage
                    listMessageProps = {listMessage}
                /> 
            </View>

            <StatusBar style="auto" />
        </KeyboardAvoidingView>
    );
}
    
export default App;