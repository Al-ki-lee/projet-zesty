# Mobile application project in React Native for Zesty

## Description
This project was created for the Zesty's technical test. It uses React Native and TypeScript.

## Installation
- First, install Expo on your computer : https://docs.expo.dev/
- Clone this project.
- Install the dependencies with the command line ‘npm install’
- Run the ‘expo start’ line command to run the project
- Install the Expo mobile application on your phone or use the emulators on the web page that opened